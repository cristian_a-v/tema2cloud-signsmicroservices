#include <iostream>
#include <SwitchService.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

#include <regex>

bool checkValidDate(std::string date) {
	std::regex regex_template("((0[13578]|1[02])[/]31[/](18|19|20)[0-9]{2})|((01|0[3-9]|1[1-2])[/](29|30)[/](18|19|20)[0-9]{2})|((0[1-9]|1[0-2])[/](0[1-9]|1[0-9]|2[0-8])[/](18|19|20)[0-9]{2})|((02)[/]29[/](((18|19|20)(04|08|[2468][048]|[13579][26]))|2000))");
	if (std::regex_match(date, regex_template)) {
		return true;
	}
	return false;
}

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = SwitchService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	SignRequest signRequest;
	std::cout << "Your Bday (mm/dd/yyyy): ";
	std::string bDay;
	std::cin >> bDay;
	if (!checkValidDate(bDay)) {
		std::cout << "Data invalida!";
		return 1;
	}

	signRequest.set_date(bDay);

	SignResponse response;
	auto status = sum_stub->GetSign(&context, signRequest, &response);
	if (status.ok()) {
		std::cout << response.sign();
	}
}

