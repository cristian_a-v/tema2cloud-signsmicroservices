// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: SpringService.proto

#include "SpringService.pb.h"
#include "SpringService.grpc.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/channel_interface.h>
#include <grpcpp/impl/codegen/client_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/rpc_service_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/sync_stream.h>

static const char* SpringService_method_names[] = {
  "/SpringService/GetSign",
};

std::unique_ptr< SpringService::Stub> SpringService::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< SpringService::Stub> stub(new SpringService::Stub(channel));
  return stub;
}

SpringService::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_GetSign_(SpringService_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status SpringService::Stub::GetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::SignResponse* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_GetSign_, context, request, response);
}

void SpringService::Stub::experimental_async::GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_GetSign_, context, request, response, std::move(f));
}

void SpringService::Stub::experimental_async::GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_GetSign_, context, request, response, std::move(f));
}

void SpringService::Stub::experimental_async::GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_GetSign_, context, request, response, reactor);
}

void SpringService::Stub::experimental_async::GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_GetSign_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::SignResponse>* SpringService::Stub::AsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::SignResponse>::Create(channel_.get(), cq, rpcmethod_GetSign_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::SignResponse>* SpringService::Stub::PrepareAsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::SignResponse>::Create(channel_.get(), cq, rpcmethod_GetSign_, context, request, false);
}

SpringService::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      SpringService_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< SpringService::Service, ::SignRequest, ::SignResponse>(
          std::mem_fn(&SpringService::Service::GetSign), this)));
}

SpringService::Service::~Service() {
}

::grpc::Status SpringService::Service::GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


