#pragma once
#include "SummerService.grpc.pb.h"
#include "ZodiacSigns.h"

class SummerServiceImp final : SummerService::Service
{
public:
	SummerServiceImp() {};
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;

};

