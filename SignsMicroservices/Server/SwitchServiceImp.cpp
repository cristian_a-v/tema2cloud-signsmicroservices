#include "SwitchServiceImp.h"
#include "SpringServiceImp.h"
#include "SummerServiceImp.h"
#include "AutumnServiceImp.h"
#include "WinterServiceImp.h"

::grpc::Status SwitchServiceImp::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{
	std::string season = ZodiacSigns::getSeason(request->date());
	::grpc::Status status;
	if (season == "_Spring") {
		SpringServiceImp spring;
		status = spring.GetSign(context, request, response);
	} else
	if (season == "_Summer") {
		SummerServiceImp summer;
		status = summer.GetSign(context, request, response);
	} else
	if (season == "_Autumn") {
		AutumnServiceImp autumn;
		status = autumn.GetSign(context, request, response);
	} else
	if (season == "_Winter") {
		WinterServiceImp winter;
		status = winter.GetSign(context, request, response);
	}

	if (status.ok())
	{
		std::cout << "\nDate: "<<request->date()<< " Season: "<<season<<" Sign: " << response->sign();
	}
	
	return ::grpc::Status::OK;
}
