#pragma once
#include <SwitchService.grpc.pb.h>
#include "ZodiacSigns.h"


class SwitchServiceImp final : public SwitchService::Service
{
public:
	SwitchServiceImp() {};
	::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override;
};

