#pragma once
#include "AutumnService.grpc.pb.h"
#include "ZodiacSigns.h"

class AutumnServiceImp final : AutumnService::Service
{
public:
	AutumnServiceImp() {};
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;

};

