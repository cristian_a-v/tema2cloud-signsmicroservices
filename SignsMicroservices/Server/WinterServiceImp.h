#pragma once
#include "WinterService.grpc.pb.h"
#include "ZodiacSigns.h"

class WinterServiceImp final : WinterService::Service
{
public:
	WinterServiceImp() {};
	virtual ::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response);

};

