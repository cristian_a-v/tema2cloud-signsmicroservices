#include "ZodiacSigns.h"

bool ZodiacSigns::dateInInterval(const std::string& date, std::pair<std::string, std::string> interval)
{
	std::vector<std::string> parse_date = ZodiacSigns::split(date, '/');
	std::vector<std::string> start_interval_date = ZodiacSigns::split(interval.first, '/');
	std::vector<std::string> end_interval_date = ZodiacSigns::split(interval.second, '/');
	if (std::stoi(parse_date.front()) == std::stoi(start_interval_date.front()) && std::stoi(parse_date.at(1)) >= std::stoi(start_interval_date.at(1)))
	{
		return true;
	}
	if (std::stoi(parse_date.front()) == std::stoi(end_interval_date.front()) && std::stoi(parse_date.at(1)) <= std::stoi(end_interval_date.at(1)))
	{
		return true;
	}
	if (std::stoi(parse_date.front()) > std::stoi(start_interval_date.front()) && std::stoi(parse_date.front()) < std::stoi(end_interval_date.front()))
	{
		return true;
	}
	return false;


}

std::vector<std::string> ZodiacSigns::split(const std::string& s, char delim)
{
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(std::move(item));
	}
	return elems;
}

void ZodiacSigns::readFromFile()
{
	std::ifstream f(file_path);
	if (!f.is_open())
	{
		return;
	}
	std::string season;
	while (!f.eof())
	{
		f >> season;
		using strings_pair = std::pair<std::string, std::string>;
		std::map < std::string, strings_pair> signs;
		if (!signs.empty())
		{
			signs.clear();
		}
		std::string sign;
		std::string start_date;
		std::string end_date;
		for (int i = 0; i < NR_SIGNS / NR_SEASONS; i++) {
			f >> sign >> start_date >> end_date;

			std::pair<std::string, std::string> period(start_date, end_date);
			signs.insert(std::pair<std::string, strings_pair>(sign, period));
		}
		seasons.insert(std::pair<std::string, std::map<std::string, strings_pair>>(season, signs));
	}
	std::cout << "\nReadFromFile Finished";
}

std::pair<std::string, std::string> ZodiacSigns::getSeasonPeriod(const std::string& season)
{
	std::string start_date = seasons.at(season).begin()->second.first; //data de inceput
	std::string end_date = seasons.at(season).rbegin()->second.second;
	return std::pair<std::string, std::string>(start_date,end_date);
}


std::string ZodiacSigns::getSeason(const std::string& date)
{
	auto iterator = seasons.begin();
	while (iterator != seasons.end()) {
		if (dateInInterval(date, getSeasonPeriod(iterator->first))) {
			return iterator->first;
		}
		iterator++;
	}
}

std::string ZodiacSigns::getSign(const std::string& date)
{
	auto signs_map = seasons.at(getSeason(date));
	auto iterator = signs_map.begin();
	while (iterator != signs_map.end()) {
		if (dateInInterval(date, iterator->second)) {
			return iterator->first;
		}
		iterator++;
	}
	
}

const std::string ZodiacSigns::file_path = "../../../Tema2Cloud_SignsMicroservices/Signs.txt";
std::map<std::string, std::map<std::string, std::pair<std::string, std::string>>> ZodiacSigns::seasons;