#pragma once
#include<iostream>
#include<unordered_map>
#include<map>
#include<fstream>
#include <string>
#include <sstream>
#include <vector>
#define NR_SEASONS 4
#define NR_SIGNS 12

class ZodiacSigns
{
private:
	static std::map<std::string,std::map<std::string,std::pair<std::string,std::string>>> seasons;
	static const std::string file_path; 
	ZodiacSigns() {};
	static std::vector<std::string> split(const std::string& s, char delim);
	static std::pair<std::string, std::string> getSeasonPeriod(const std::string& season);
	static bool dateInInterval(const std::string& date, std::pair<std::string, std::string> interval);
public:
	static void readFromFile();
	static std::string getSeason(const std::string& date);
	static std::string getSign(const std::string& date);

};

