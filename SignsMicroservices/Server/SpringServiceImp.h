#pragma once
#include <SpringService.grpc.pb.h>
#include "ZodiacSigns.h"

class SpringServiceImp final : public SpringService::Service
{
public:
	SpringServiceImp() { };
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;

};

