#include "WinterServiceImp.h"

::grpc::Status WinterServiceImp::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{
	std::string sign = ZodiacSigns::getSign(request->date());
	response->set_sign(sign);
	return ::grpc::Status::OK;
}
